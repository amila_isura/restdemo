/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amila.security.app.securityconfig;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.csrf.InvalidCsrfTokenException;

/**
 *
 * @author cosmoforge
 */
//public class AuthFailureHandler implements AuthenticationEntryPoint {
//
//  @Override
//  public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e)
//      throws IOException, ServletException {
//    httpServletResponse.setContentType("application/json");
//    httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
//
//    if( e instanceof InsufficientAuthenticationException) {
//
//      if( e.getCause() instanceof InvalidCsrfTokenException ){
//        httpServletResponse.getOutputStream().println(
//            "{ "
//                + "\"message\": \"Token has expired\","
//                + "\"type\": \"Unauthorized\","
//                + "\"status\": 401"
//                + "}");
//      }
//    }
//    if( e instanceof AuthenticationCredentialsNotFoundException) {
//
//      httpServletResponse.getOutputStream().println(
//          "{ "
//              + "\"message\": \"Missing Authorization Header\","
//              + "\"type\": \"Unauthorized\","
//              + "\"status\": 401"
//              + "}");
//    }
//
//  }
//}
