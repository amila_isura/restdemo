/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amila.security.app.dao;

import com.amila.security.app.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author amila
 */
public interface UserRepository extends JpaRepository<User, Long>{
    

}