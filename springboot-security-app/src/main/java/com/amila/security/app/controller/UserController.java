/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.amila.security.app.controller;

import java.security.Principal;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author cosmoforge
 */
@RestController
public class UserController {

    @RequestMapping("/userlogin")
    public String userValidation() {
        return "User, Successfully logged in!";
    }

    @RequestMapping("/adminlogin")
    public String adminValidation() {
        return "Admin user, Successfully logged in!";
    }
    
    @RequestMapping(value = "/welcome", method = RequestMethod.GET)
    @ResponseBody
    public String welcomeMessage(Principal principal){
        return "Welcome "+ principal.getName();
    }
    
    @RequestMapping(value = "/manage", method = RequestMethod.GET)
    @ResponseBody
    @PreAuthorize("hasPermission(principal, 'READ')")
    public String manage(Principal principal){
        return "Manage ";
    }
    
    @RequestMapping(value = "/group")
    @PreAuthorize("hasPermission(filterObject, 'READ')")
    public String group(){
        return "Group ";
    }
}
