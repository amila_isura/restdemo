/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  cosmoforge
 * Created: Jan 11, 2022
 */

INSERT INTO acl_sid (id, principal, sid) VALUES
(1, 0, 'default,'),
(2, 0, 'work'),
(3, 0, 'manage'),
(4, 0, 'group');

INSERT INTO acl_class (id, class) VALUES
(1, 'com.amila.security.app.entity.User');

INSERT INTO users(id,username,password,enabled) VALUES 
(1,'bob','bob',true),
(2,'alice','alice',true),
(3,'charlie','charlie',true);

INSERT INTO authorities(username,authority)VALUES
('bob','manage'),
('alice','group');

INSERT INTO acl_object_identity (id, object_id_class, object_id_identity, parent_object, owner_sid, entries_inheriting) VALUES
(1, 1, 1, NULL, 3, 0),
(2, 1, 2, NULL, 4, 0);

INSERT INTO acl_entry (id, acl_object_identity, ace_order, sid, mask, granting, audit_success, audit_failure) VALUES
(1, 1, 1, 3, 1, 1, 1, 1),
(2, 2, 1, 4, 1, 1, 1, 1);

